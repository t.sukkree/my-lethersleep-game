package com.lethersleep.game;

import java.util.Random;

public class Object {
	
	private int num = LetHerSleep.NUMOBJ;
	private int [][][] object = new int [2][3][num];
	private int [] checkPosX = new int [num * 2];
	private int fixedPosXValue = 5;
	private int moveStep = 0;
	private boolean canUpdateNow = false;
	private boolean wasNextToNowDone = false;
	private boolean moveDone = true;
	private static int NOWOBJ = 0;
	private static int NEXTOBJ = 1;
	Random r = new Random();
	
	public Object() {
		initObject();
	}

	void initObject() {
		object[NOWOBJ][1][0] = fixedPosXValue;
		genObjCollection(NOWOBJ);
		genNextObj();
	}
	
	private void genNextObj() {
		object[NEXTOBJ][1][0] = 1005;
		genObjCollection(NEXTOBJ);
	}
	
	private void genObjCollection(int type) {
		int count = 0;
		object[type][0][0] = 0;
		object[type][2][0] = LetHerSleep.HEIGHT;
		for (int i = 1; i < num; i++) {
			object[type][2][i] = LetHerSleep.HEIGHT;
			object[type][1][i] = fixedPosXValue + ((i + (num * type)) * (fixedPosXValue + LetHerSleep.OBJSIZE));
			if (object[type][0][i - 1] == 1) {
				object[type][0][i] = 0;
			} else if (count > 0 || object[type][0][i - 1] == 2) {
				object[type][0][i] = r.nextInt(2);
			} else if (object[type][0][i - 1] == 0 ) {
				object[type][0][i] = r.nextInt(4);
				count++;
			} 
		}
	}
	
	int[][][] getObjCollection() {
		return object;
	 } 
	
	public void update() {
		updateMove();
		if (moveDone && moveStep != 0) {
			moveDone = false;
			move(moveStep);
		}
	}
	
	public void updateMove() {
		if (canUpdateNow == true) {
			for (int k = 0; k < 2; k++) {	
				for (int i = 0; i < num; i++) {
					if (object[k][1][i] != checkPosX[i]) {
						object[k][1][i] -= (moveStep * 25);
					}
				}
			}
			if (object[NOWOBJ][1][num - 1] == checkPosX[num - 1]) {
				canUpdateNow = false;
				if (object[NEXTOBJ][1][0] == fixedPosXValue && !wasNextToNowDone) {
					nextToNow(0);
				}
				moveDone = true;
				moveStep = 0;
			}
		}
	}
	
	private void move(int step) {
		wasNextToNowDone = false;
		if (object[NOWOBJ][1][num - 1] == fixedPosXValue && step == 2) {
			nextToNow(1);
			wasNextToNowDone = true;
		}
		for(int k = 0; k < 2; k++){
			for (int i = 0; i < num; i++) {
				checkPosX[i + (num * k)] = object[k][1][i] - (step * (fixedPosXValue + LetHerSleep.OBJSIZE));
			}	
		}
		canUpdateNow = true;
	}
	
	public void setNextMove(int step) {
		if (moveDone) {
			moveStep = step;
		}
	}
	
	public int checkObjectAndDisappear() {
		for (int i = 0; i < num; i++) {
			for(int k = 0; k < 2; k++) {
				if (object[k][1][i] == fixedPosXValue) {
					object[k][2][i] = -500;
					return object[k][0][i];
				}
			}
		}
		return 0;
	}
	
	private void nextToNow(int k) {	
		for (int i = 0; i < 3; i++) {
			if (k == 1) {
				object[NOWOBJ][i][0] = object[NOWOBJ][1][num - 1];
			} else if (k == 0) {
				object[NOWOBJ][i][0] = object[NEXTOBJ][i][0];
			}
			for (int j = 1; j < num; j++) {
				object[NOWOBJ][i][j] = object[NEXTOBJ][i][j - k];
			}
		}
		genNextObj();
	}
	
}