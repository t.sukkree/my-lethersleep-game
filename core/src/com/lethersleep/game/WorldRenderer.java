package com.lethersleep.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer {
	
	private LetHerSleep letHerSleep;
	private World world;
	private Texture sleeperImg, background, howtoImg;
	private SpriteBatch batch;
	private ObjectRenderer objectRenderer;
	private Sleeper sleeper;
	private SoundEffect sound;
	private BitmapFont font, scoreText, scoreText2, bonusText;
	Texture sleeperSleep, sleeperWoken;
	FreeTypeFontGenerator generator; 
	FreeTypeFontParameter parameter;
	
	public WorldRenderer(LetHerSleep letHerSleep, World world) {
        this.letHerSleep = letHerSleep;
        objectRenderer = new ObjectRenderer(letHerSleep.batch, world.getObject());
        batch = letHerSleep.batch;
        this.world = world;
        sleeper = world.getSleeper();
        sound = world.getSound();
        sleeperSleep = new Texture("sleeper.png");
        sleeperWoken = new Texture("sleeper-woken.png");
        howtoImg = new Texture("howto.jpg");
        background = new Texture("bg.png");
        genFont();
    }
	
	private void genFont() {
		 generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/lucon.ttf"));
	     parameter = new FreeTypeFontParameter();
	     parameter.size = 40;
	     parameter.color = Color.BLACK;
	     font = generator.generateFont(parameter); 
	     parameter.size = 40;
	     parameter.color = Color.YELLOW;
	     bonusText = generator.generateFont(parameter); 
	     genFontScoreScreen();
	     generator.dispose();
	}
	
	private void genFontScoreScreen() {
		parameter.size = 50;
	    parameter.color = Color.WHITE;
	    scoreText2 = generator.generateFont(parameter);
	    parameter.size = 25;
	    parameter.color = Color.BLACK;
	    scoreText = generator.generateFont(parameter);
	}
	
	public void update(float delta) {
		sound.playBackgroundMusic();
		if (sleeper.getState() == sleeper.SLEEPER_SLEEP) {
			sleeperImg = sleeperSleep;
		} else if (sleeper.getState() == sleeper.SLEEPER_WOKEN) {
			sleeperImg = sleeperWoken;
		}
	}
	
	public void render(float delta) {
		update(delta);
		batch.begin();
		batch.draw(background, 0, 0);
		batch.end();
        if (letHerSleep.GAME_STATE == letHerSleep.PLAY_STATE) {
        	renderPlayScreen();
        } else if (letHerSleep.GAME_STATE == letHerSleep.SHOW_SCORE) {
        	renderScoreScreen();
        } else {
        	renderHowToScreen();
        }
	}
	
	private void renderPlayScreen(){
		objectRenderer.render();
        batch.begin();
        batch.draw(sleeperImg, sleeper.getPos().x, sleeper.getPos().y);
        font.draw(batch, "SCORE", 850, 585);
        font.draw(batch, "" + world.getScore(), 890, 540);
        font.draw(batch, "TIME", 28, 585);
        font.draw(batch, "" + world.getTime(), 50, 540);
        if (world.getCheckBonus()) {
        	bonusText.draw(batch, "SCORE x2", 440, 400);
        }
        batch.end();       
	}
	
	private void renderHowToScreen() {
		batch.begin();
		batch.draw(howtoImg, 0, 0);
		batch.end();
	}
	
	private void renderScoreScreen(){
		batch.begin();
		scoreText2.draw(batch, "SCORE", 430, 400);
		scoreText2.draw(batch, "" + world.getScore(), 480, 320);
		scoreText.draw(batch, "press spacebar to play again", 300, 240);
		batch.end();
	}

}
