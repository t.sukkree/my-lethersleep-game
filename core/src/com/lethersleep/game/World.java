package com.lethersleep.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;

public class World {
	
	private Object object;
	private Sleeper sleeper;
	private SoundEffect sound;
	private int score = 0;
	private double time = 20;
	private double bonusTime = 0;
	private int bonusPoint = 1;
	private boolean startBonusTime = false;
	private static final int GOODOBJ = 0;
	private static final int BADOBJ = 1;
	private static final int ADDTIME = 2;
	private LetHerSleep lethersleep;
	
	public World(LetHerSleep lethersleep) {
		object = new Object();
		sound = new SoundEffect();
		sleeper = new Sleeper(5, LetHerSleep.HEIGHT, this);
		this.lethersleep = lethersleep;
	}
	
	Object getObject() {
		return object;
	}
	
	Sleeper getSleeper() {
		return sleeper;
	}
	
	SoundEffect getSound() {
		return sound;
	}
	
	int getScore() {
		return score;
	}
	
	int getTime() {
		return (int)time;
	}
	
	boolean getCheckBonus() {
		return startBonusTime;
	}
	
	void update(float delta) {
		sleeper.update();
		object.update();
		updateBonus(delta);
		time -= delta;
		if((int)time == 0){
			lethersleep.GAME_STATE = LetHerSleep.SHOW_SCORE;
		}
	}
	
	private void updateBonus(float delta){
		if (startBonusTime) {
			bonusTime += delta;
			bonusPoint = 2;
			if ((int)bonusTime == 5) {
				bonusPoint = 1;
				startBonusTime = false;
			} 
		}
	}
	
	void sleeperGotObject() {
		int objectValue = object.checkObjectAndDisappear();
		if (objectValue == GOODOBJ) {
			sound.playBumpSound();
			updateScore(1*bonusPoint);
		} else if (objectValue == BADOBJ) {
			sound.playBumpToAlarmSound();
	        sleeper.SLEEPER_STATE = sleeper.SLEEPER_WOKEN;
			if (score > 5) {
				updateScore(-10);
			} else {
				clearScore();
			}
		} else if (objectValue == ADDTIME) {
			sound.playSpecialItemSound();
			time += 5;
		} else {
			sound.playSpecialItemSound();
			startBonusTime = true;
			bonusTime = 0;
		}
	}
	
	void updateScore(int add) {
		score += add;
	}
	
	void clearScore() {
		score = 0;
	}
	
	void reset() {
		time = 20;
		score = 0;
		object.initObject();
	}
	
}
