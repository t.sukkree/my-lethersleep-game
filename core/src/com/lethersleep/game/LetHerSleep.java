package com.lethersleep.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LetHerSleep extends Game {
	
	public SpriteBatch batch;
	public static final int NUMOBJ = 8;
	public static final int OBJSIZE = 120;
	public static final int HEIGHT = 40;
	public int GAME_STATE = 0;
	public static final int HOW_TO = 0;
	public static final int PLAY_STATE = 1;
	public static final int SHOW_SCORE = 2;
	
	@Override
	public void create() {
		batch = new SpriteBatch();
		setScreen(new GameScreen(this));
	}

	@Override
	public void render() {
		super.render();
	}
	
	@Override
	public void dispose() {
		batch.dispose();
	}
	
}





