package com.lethersleep.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;

public class GameScreen extends ScreenAdapter {
	 
	private LetHerSleep lethersleep;
	private WorldRenderer worldRenderer;
	private World world;
	 
	public GameScreen(LetHerSleep lethersleep) {
		 this.lethersleep = lethersleep;
	     world = new World(lethersleep);
	     worldRenderer = new WorldRenderer(lethersleep,world);
	}
	 
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(255, 255, 255, 1);
	    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	    update(delta);
	    world.update(delta);
	    worldRenderer.render(delta);
	}
	 
	public void update(float delta) {
		if (lethersleep.GAME_STATE == lethersleep.PLAY_STATE) {
			if (Gdx.input.isKeyJustPressed(Keys.NUM_1)) {
				world.getObject().setNextMove(1);
				world.getSleeper().jump();
			} else if (Gdx.input.isKeyJustPressed(Keys.NUM_2)) {
				world.getObject().setNextMove(2);
				world.getSleeper().jump();
			}
	    }	
		else{
			if (Gdx.input.isKeyJustPressed(Keys.SPACE)) {
				lethersleep.GAME_STATE = lethersleep.PLAY_STATE;
				world.reset();
			}
		}
	}
	
}
