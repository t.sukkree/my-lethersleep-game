package com.lethersleep.game;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.Texture;

public class ObjectRenderer {
	
	private Object object;
	private SpriteBatch batch;
	private Texture goodObj, badObj, addTimeObj, bonusObj;
	private int [][][] objCollection;
	
	public ObjectRenderer(SpriteBatch batch, Object object) {
		this.object = object;
		this.batch = batch;
		goodObj = new Texture("good.png");
		badObj = new Texture("bad.png");
		addTimeObj = new Texture("addtime.png");
		bonusObj = new Texture("bonus.png");
	}
	
	public void render() {
		batch.begin();
		objCollection = object.getObjCollection();
		for(int k = 0; k < 2; k++){
			for (int i = 0; i < LetHerSleep.NUMOBJ; i++) {
				if (objCollection[k][0][i] == 0) {
					batch.draw(goodObj, objCollection[k][1][i], objCollection[k][2][i]);
				} else if (objCollection[k][0][i] == 1) {
					batch.draw(badObj, objCollection[k][1][i], objCollection[k][2][i]);
				} else if (objCollection[k][0][i] == 2) {
					batch.draw(addTimeObj, objCollection[k][1][i], objCollection[k][2][i]);
				} else {
					batch.draw(bonusObj, objCollection[k][1][i], objCollection[k][2][i]);
				}			
			}
		}
		batch.end();
	}
	
}
