package com.lethersleep.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundEffect {

	Music bgSound, bumpToAlarmSound; 
	Sound bumpSound, specialItemSound;

	public SoundEffect() {
		bgSound = Gdx.audio.newMusic(Gdx.files.internal("sound/backgroundmusic.mp3"));
		bumpSound = Gdx.audio.newSound(Gdx.files.internal("sound/bump.mp3"));
		bumpToAlarmSound = Gdx.audio.newMusic(Gdx.files.internal("sound/alarm.mp3"));
		specialItemSound = Gdx.audio.newSound(Gdx.files.internal("sound/wink.mp3"));
		bgSound.setLooping(true);
		bgSound.setVolume(0.27f);
		bumpToAlarmSound.setVolume(0.6f);
	}
	
	public void playBackgroundMusic() {
		bgSound.play();
	}
	
	public void playBumpSound() {
		bumpSound.play();
	}
	
	public void playBumpToAlarmSound() {
		bumpToAlarmSound.play();
	}
	
	public void playSpecialItemSound() {
		specialItemSound.play();
	}
	
}
