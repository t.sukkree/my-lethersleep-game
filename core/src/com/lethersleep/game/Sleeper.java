package com.lethersleep.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class Sleeper {
	
	public static final int UP = LetHerSleep.HEIGHT;
	public static final int DOWN = -(LetHerSleep.HEIGHT);
	public static final int STILL = 0;
	public static final int SLEEPER_WOKEN = 1;
	public static final int SLEEPER_SLEEP = 0;
	public int SLEEPER_STATE = 0;
	private Vector2 position;
	private int sleeperAction;
	private World world;
	
	public Sleeper(int x, int y, World world) {
		position = new Vector2(x,y);
		sleeperAction = STILL;
		this.world = world;
	}
	
	Vector2 getPos() {
		return position;
	}
	
	int getState() {
		return SLEEPER_STATE;
	}
	
	public void jump() {
		sleeperAction = UP;
		SLEEPER_STATE = SLEEPER_SLEEP;
	}
	
	public void update() {
		if (position.y != (5 * LetHerSleep.HEIGHT) && sleeperAction == UP) {
			position.y += UP;
		} else if (position.y == (5 * LetHerSleep.HEIGHT) && sleeperAction == UP) {
			sleeperAction = DOWN;
		} else if (position.y != LetHerSleep.HEIGHT && sleeperAction == DOWN) {
			position.y += DOWN;
			if (position.y == (3 * LetHerSleep.HEIGHT)) {
				world.sleeperGotObject();
			}
		} else if (position.y == LetHerSleep.HEIGHT && sleeperAction == DOWN) {
			sleeperAction = STILL;
		}
	}
	
}
